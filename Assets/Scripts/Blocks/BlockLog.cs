using UnityEngine;
public class BlockLog : Block {
    public BlockLog(byte ID) : base(ID,"Log",Vector2Int.zero){}
    public override void AddFaceTexture(Face face,ref MeshData md,ChunkController c,Vector3Int blockPos) {
        if (face == Face.Up || face == Face.Down) {
            
                md.AddUVs(TextureUtil.GetTileUVs(new Vector2Int(5,0)));
        } else {
                md.AddUVs(TextureUtil.GetTileUVs(new Vector2Int(4,0)));

        }
    }
}