using UnityEngine;
public class BlockGrass :Block {
    public BlockGrass(byte ID) :base(ID,"Grass",Vector2Int.zero){}

    public override void AddFaceTexture(Face face,ref MeshData md,ChunkController c,Vector3Int blockPos) { 
            if (face == Face.Up) {
                md.AddUVs(TextureUtil.GetTileUVs(new Vector2Int(1,0)));
            } else if (face== Face.Down) {
                md.AddUVs(TextureUtil.GetTileUVs(new Vector2Int(2,0)));

            } else {
                
                md.AddUVs(TextureUtil.GetTileUVs(new Vector2Int(3,0)));
            }

    }


}