﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block 
{
    public enum Face {
        Up,
        Down,
        Front,
        Back,
        Left,
        Right
    }
    public static Block[] Blocks = new Block[]{
        new BlockAir(0,"Air"),
        new Block(1,"Stone",new Vector2Int(0,0)),
        new Block(2,"Dirt",new Vector2Int(2,0)),
        new BlockGrass(3),
        new BlockLog(4)
    };

    private byte _id;
    private string _name;
    public string Name {get{return _name;}}
    private Vector2Int  _textureCoOrd;
    public Block(byte ID, string Name,Vector2Int TextureCoOrd) {
        this._id = ID;
        this._name = Name;
        this._textureCoOrd = TextureCoOrd;
    }
    public virtual bool isSolid (ChunkController chunk, Vector3Int blockpos) { return true;}

    public virtual void AddFaceTexture(Face face,ref MeshData md,ChunkController c,Vector3Int blockPos) {
        md.AddUVs(TextureUtil.GetTileUVs(_textureCoOrd));

    }
    public virtual MeshData GetModel(ChunkController c,Vector3Int blockPos) {
        MeshData md = new MeshData();
             if (WorldController.MainWorld.GetBlock(blockPos+Vector3Int.up+c.pos) < 255 && !Block.Blocks[WorldController.MainWorld.GetBlock(blockPos+Vector3Int.up+c.pos)].isSolid(c,blockPos + Vector3Int.up)) {
                //build upper face
                Vector3 topLeft = new Vector3(0f,1f,0f)+blockPos;
                Vector3 topRight = new Vector3(1f,1f,0f)+blockPos;
                Vector3 bottomRight = new Vector3(1f,1f,1f)+blockPos;
                Vector3 bottomLeft = new Vector3(0f,1f,1f)+blockPos;
                md.AddQuad(topLeft,topRight,bottomLeft,bottomRight,Vector3.up);
                AddFaceTexture(Face.Up,ref md,c,blockPos);
            }
            if(WorldController.MainWorld.GetBlock(blockPos+Vector3Int.down +c.pos) < 255 && !Block.Blocks[WorldController.MainWorld.GetBlock(blockPos+Vector3Int.down+c.pos)].isSolid(c,blockPos + Vector3Int.down)) {
                //build lower face
                
                Vector3 topLeft = new Vector3(0f,0f,1f)+blockPos;
                Vector3 topRight =new Vector3(1f,0f,1f)+blockPos;
                Vector3 bottomRight = new Vector3(1f,0f,0f)+blockPos;
                Vector3 bottomLeft = new Vector3(0f,0f,0f)+blockPos;
                AddFaceTexture(Face.Down,ref md,c,blockPos);

                md.AddQuad(topLeft,topRight,bottomLeft,bottomRight,Vector3.down);
            }
            if(WorldController.MainWorld.GetBlock(blockPos+Vector3Int.left+c.pos) < 255 && !Block.Blocks[WorldController.MainWorld.GetBlock(blockPos+Vector3Int.left+c.pos)].isSolid(c,blockPos + Vector3Int.left)) {
                //build left face
                Vector3 topLeft = new Vector3(0f,0f,1f)+blockPos;
                Vector3 topRight =new Vector3(0f,0f,0f)+blockPos;
                Vector3 bottomRight = new Vector3(0f,1f,0f)+blockPos;
                Vector3 bottomLeft = new Vector3(0f,1f,1f)+blockPos;
                md.AddQuad(topLeft,topRight,bottomLeft,bottomRight,Vector3.left);
                AddFaceTexture(Face.Left,ref md,c,blockPos);
            }
            if(WorldController.MainWorld.GetBlock(blockPos+Vector3Int.right+c.pos) < 255 && !Block.Blocks[WorldController.MainWorld.GetBlock(blockPos+Vector3Int.right+c.pos)].isSolid(c,blockPos + Vector3Int.right)) {
                //build right face
                
                Vector3 topLeft = new Vector3(1f,0f,0f)+blockPos;
                Vector3 topRight =new Vector3(1f,0f,1f)+blockPos;
                Vector3 bottomRight = new Vector3(1f,1f,1f)+blockPos;
                Vector3 bottomLeft = new Vector3(1f,1f,0f)+blockPos;
                
                AddFaceTexture(Face.Right,ref md,c,blockPos);

                md.AddQuad(topLeft,topRight,bottomLeft,bottomRight,Vector3.right);
            }
            if(WorldController.MainWorld.GetBlock(blockPos+Util.Vector3Forward+c.pos) < 255 && !Block.Blocks[WorldController.MainWorld.GetBlock(blockPos+Util.Vector3Forward+c.pos)].isSolid(c,blockPos + Util.Vector3Forward)) {
                //build front face
                
                Vector3 topLeft = new Vector3(1f,0f,1f)+blockPos;
                Vector3 topRight =new Vector3(0f,0f,1f)+blockPos;
                Vector3 bottomRight = new Vector3(0f,1f,1f)+blockPos;
                Vector3 bottomLeft = new Vector3(1f,1f,1f)+blockPos;

                AddFaceTexture(Face.Front,ref md,c,blockPos);


                md.AddQuad(topLeft,topRight,bottomLeft,bottomRight,Util.Vector3Forward);
            }
            if(WorldController.MainWorld.GetBlock(blockPos+Util.vector3IntBackward+c.pos) < 255 && !Block.Blocks[WorldController.MainWorld.GetBlock(blockPos+Util.vector3IntBackward+c.pos )].isSolid(c,blockPos + Util.vector3IntBackward)) {
                //build back face
                
                Vector3 topLeft = new Vector3(0f,0f,0f)+blockPos;
                Vector3 topRight =new Vector3(1f,0f,0f)+blockPos;
                Vector3 bottomRight = new Vector3(1f,1f,0f)+blockPos;
                Vector3 bottomLeft = new Vector3(0f,1f,0f)+blockPos;

                AddFaceTexture(Face.Back,ref md,c,blockPos);
                md.AddQuad(topLeft,topRight,bottomLeft,bottomRight,Util.vector3IntBackward);
            }
        return md;

    }
}
