using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockAir : Block {
    public override bool isSolid(ChunkController chunk, Vector3Int blockpos) {return false;}
    public override MeshData GetModel(ChunkController c,Vector3Int blockPos) {return new MeshData();}
    public BlockAir(byte id, string name) :base(id,name,Vector2Int.zero){}
}
