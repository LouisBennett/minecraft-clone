using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public static class TextureUtil {

    public const int NUM_TILES = 16;

    public const float TILE_LENGTH = 1/(float)NUM_TILES;
    
    public static Vector2[] GetTileUVs(Vector2Int tileCoord) {
        float tx = (tileCoord.x * TILE_LENGTH);
        float ty = (NUM_TILES - tileCoord.y);
        ty *= TILE_LENGTH;
        Vector2[] v = new Vector2[4];
        v[0] = new Vector2(tx,ty-TILE_LENGTH); //up left corner
        v[1] = new Vector2(tx+TILE_LENGTH,ty-TILE_LENGTH); //up right corner
        
        v[2] = new Vector2(tx,ty); //bottom left corners
        v[3] = new Vector2(tx+TILE_LENGTH,ty); //bottom right cornery
        return v;
    }
}