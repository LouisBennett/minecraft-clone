﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class ChunkController : MonoBehaviour
{
    public enum ChunkState {
        Fresh, 
        GeneratingData,
        GeneratedData,
        GeneratingMesh,
        GeneratedMesh,
        Clean,
        Dirty
    }
    public byte[] Blocks;
    private byte[] BlockData;

    private MeshData meshData;
    public ChunkState cstate;

    public Vector3 pos;
    public Vector3 NoisePos;
    
    // Start is called before the first frame update
    void Start()
    {
        cstate = ChunkState.Fresh;
        this.Blocks = new byte[WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkHeight];
        this.BlockData = new byte[WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkHeight];
        meshData = new MeshData();
        pos = transform.position;
    }
    public void GenerateData() {
        cstate = ChunkState.GeneratingData;
        if (this.Blocks == null)
            this.Blocks = new byte[WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkHeight];
        for (int x = 0; x < WorldController.MainWorld.ChunkSize; x++)
        {
                for (int z = 0; z < WorldController.MainWorld.ChunkSize; z++)
                {
                    int height = 1;
                    float value;
                        value = WorldController.fastNoiseLite.GetNoise((pos.x+x+10000)*0.02f,(pos.z+z+10000)*0.02f);
                        value *= 0.5f;
                        value += 0.5f;
                    height = Mathf.RoundToInt(value*(float)(WorldController.MainWorld.ChunkHeight/4f)+(WorldController.MainWorld.ChunkHeight/2f));
                    int stoneHeight = height - 3;
                    stoneHeight = height - stoneHeight;
                    for (int y = 0; y<WorldController.MainWorld.ChunkHeight;y++) { 
                        byte b = 0;
                        if (y==height) {
                            b = 3;
                        } else if (y<height){
                            if (y>stoneHeight) {
                                b = 2;
                            } else {
                                b = 1;
                            }
                        }


                        SetBlock(x,y,z,b);
                        SetBlockData(x,y,z,0);
                    }
                    
                }
            
        }
        
        cstate = ChunkState.GeneratedData;
    }
    public void GenerateMesh() {
        
            meshData.ClearData();
            for (int x = 0; x < WorldController.MainWorld.ChunkSize; x++)
            {
                for (int y = 0; y <WorldController.MainWorld.ChunkHeight; y++) {
                    for (int z = 0; z < WorldController.MainWorld.ChunkSize; z++)
                    {
                        byte b;
                            b = GetBlock(x,y,z);
                        
                        meshData.JoinMeshData(Block.Blocks[b].GetModel(this,new Vector3Int(x,y,z)));
                    }
            }
        }
        Vector3 v = new Vector3(WorldController.MainWorld.ChunkSize/2f,0,WorldController.MainWorld.ChunkSize/2f);
        cstate = ChunkState.GeneratedMesh; 
    }
    public byte GetBlock(int x, int y,int z) {
        if (x < 0 || x >= WorldController.MainWorld.ChunkSize || y < 0 || y>=WorldController.MainWorld.ChunkHeight || z<0 || z >= WorldController.MainWorld.ChunkSize)
        
            return 255;
            lock (Blocks) {
                return Blocks[y * (WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkSize) + (x * WorldController.MainWorld.ChunkSize) + z];
            }
    }
    public bool SetBlock(int x, int y,int z,byte blockID) {
        if (x < 0 || x >= WorldController.MainWorld.ChunkSize || y < 0 || y>=WorldController.MainWorld.ChunkHeight || z<0 || z >= WorldController.MainWorld.ChunkSize)
            return false;
                Blocks[ (y * WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkSize) + (x * WorldController.MainWorld.ChunkSize) + z] = blockID;
                
         return true;
    }
    public byte GetBlock(Vector3Int pos) {
        if (pos.x < 0 || pos.x >= WorldController.MainWorld.ChunkSize ||pos. y < 0 || pos.y>=WorldController.MainWorld.ChunkHeight || pos.z<0 || pos.z >= WorldController.MainWorld.ChunkSize)
           {
                return 255;
           }
           
            lock (Blocks) {
            return Blocks[(pos.y * WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkSize) + (pos.x * WorldController.MainWorld.ChunkSize) + pos.z];
            }
    }
    public byte GetBlockData(int x, int y,int z) {
        return BlockData[x + (y * WorldController.MainWorld.ChunkSize) + (z * WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkHeight)];
    }
    
    public byte GetBlockData(Vector3Int pos) {
        return BlockData[pos.x + (pos.y * WorldController.MainWorld.ChunkSize) + (pos.z * WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkHeight)];
    }
    
    public void SetBlockData(int x, int y,int z,byte data) {
         BlockData[x + (y * WorldController.MainWorld.ChunkSize) + (z * WorldController.MainWorld.ChunkSize * WorldController.MainWorld.ChunkHeight)] = data;
    }
    public void SetMesh() {
        MeshFilter mf = GetComponent<MeshFilter>();
        mf.sharedMesh = meshData.toMesh();
        MeshCollider mc = GetComponent<MeshCollider>();
        mc.sharedMesh = mf.sharedMesh;
        cstate = ChunkState.Clean;
    }
    // Update is called once per frame
    void Update()
    {
    }
}
