﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
public class WorldController : MonoBehaviour
{
    public static WorldController MainWorld;
    public static FastNoiseLite fastNoiseLite;
    public int ChunkSize;
    public int ChunkHeight;
    public Material ChunkMat;
    public int WorldSeed;
    public int WorldSizeInChunks;
    public Transform Player;
    public GameObject messageBox;
    public GameObject waila;

    
    Dictionary<Vector2Int,ChunkController> WorldChunks;
    private int totalWorldSize;
    // Start is called before the first frame update

    private bool setPlayer = false;
    private bool isRunning = true;

    private Queue<ChunkController> meshGenerationQueue = new Queue<ChunkController>();

    void OnApplicationQuit() {
        isRunning = false;
    }
    void Start()
    {
        WorldSeed = System.DateTime.Now.GetHashCode();
        isRunning = true;
        fastNoiseLite = new FastNoiseLite(WorldSeed);
        fastNoiseLite.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
        fastNoiseLite.SetFrequency(1.2f);
        fastNoiseLite.SetFractalOctaves(8);
        totalWorldSize = WorldSizeInChunks *ChunkSize; 
        WorldChunks = new Dictionary<Vector2Int, ChunkController>();
        WorldController.MainWorld = this;
        
        Vector3[] barlocs = new Vector3[] {
            new Vector3(totalWorldSize/2f,ChunkHeight/2f,0f),
            new Vector3(totalWorldSize/2f,ChunkHeight/2f,totalWorldSize),
            new Vector3(0,ChunkHeight/2,totalWorldSize/2),
            new Vector3(totalWorldSize,ChunkHeight/2,totalWorldSize/2)
        };
        Vector3[] barrots = new Vector3[] {
            new Vector3(0,180,0),
            new Vector3(0,0,0),
            new Vector3(0,-90,0),
            new Vector3(0,90,0)
        };
        for (int ii=0; ii<barlocs.Length;ii++) {

            //lets create some invisible walls, people love those
            GameObject bar = GameObject.CreatePrimitive(PrimitiveType.Quad);
            bar.transform.localScale = new Vector3(totalWorldSize,ChunkHeight+20f,1f);
            bar.transform.eulerAngles = barrots[ii];
            bar.transform.position = barlocs[ii];
            bar.name = $"Barrier: {ii}";
            bar.transform.parent = this.transform;
            Destroy(bar.GetComponent<MeshRenderer>());

        }


        //shitty world creator for now
        for (int x = 0; x < WorldSizeInChunks; x++)
        {
            for (int z = 0; z < WorldSizeInChunks; z++)
                {
                    GameObject go = new GameObject($"Chunk ({x},{z})");
                    ChunkController c = go.AddComponent<ChunkController>();
                    go.GetComponent<MeshRenderer>().material= ChunkMat;
                    go.transform.position = new Vector3(x*ChunkSize,0,z*ChunkSize);
                    go.transform.parent = this.transform;
                    WorldChunks.Add(new Vector2Int(x,z),c);
                    go.layer = LayerMask.NameToLayer("Chunk");
                
            }
        }

        Thread t = new Thread(new ThreadStart(GenerateChunks));
        t.Start();

        Thread mg = new Thread(new ThreadStart(MeshGenerator));
        mg.Start();
    }
    void MeshGenerator() {
        while(isRunning) {
            ChunkController c = null;
            lock(meshGenerationQueue) {
                if (meshGenerationQueue.Count>0) {
                    c = meshGenerationQueue.Dequeue();
                }
            }
            if (c != null && c.cstate==ChunkController.ChunkState.GeneratingMesh) {
                Debug.Log($"Generating chunk: {c.pos}");
                c.GenerateMesh();
            }
        }
    }
    private void GenerateChunks() {
        foreach(KeyValuePair<Vector2Int,ChunkController> c in WorldChunks) {
            if (c.Value.cstate == ChunkController.ChunkState.Fresh) {
                c.Value.cstate = ChunkController.ChunkState.GeneratingData;
              c.Value.GenerateData();
            }
            
        }

    }
    public byte GetBlock(Vector3 pos) {
        Vector2Int chunkPos = new Vector2Int(Mathf.FloorToInt(pos.x / (float)ChunkSize),Mathf.FloorToInt(pos.z / (float)ChunkSize));
        Vector3Int blockPos = new Vector3Int(Mathf.FloorToInt(Mathf.Abs(pos.x % (float)ChunkSize)), Mathf.FloorToInt(pos.y), Mathf.FloorToInt((Mathf.Abs(pos.z % ChunkSize)))); 
        ChunkController c;
        if (WorldChunks.TryGetValue(chunkPos,out c)) {
            return c.GetBlock(blockPos);    
        } else {
            return 255;
        }
    }
    public ChunkController GetChunk(Vector3 pos) {
        Vector2Int chunkPos = new Vector2Int(Mathf.FloorToInt(pos.x / (float)ChunkSize),Mathf.FloorToInt(pos.z / (float)ChunkSize));
        ChunkController c;
        if (WorldChunks.TryGetValue(chunkPos,out c)) {
            return c;
        }
        return null;


    }
    

    // Update is called once per frame
    void Update()
    {
        if (!setPlayer) {
            
            Vector3 playerpos = new Vector3(8f,ChunkHeight+20f,8f);
            ChunkController c = GetChunk(playerpos);
            if (c != null && c.cstate == ChunkController.ChunkState.Clean)
            {
                Player.position = playerpos;
                messageBox.SetActive(false);
                waila.SetActive(true);
                setPlayer = true;
                
                Player.gameObject.GetComponent<PlayerController>().enabled = true;
            }
        }
        foreach(KeyValuePair<Vector2Int,ChunkController> c in WorldChunks) {
             if (c.Value.cstate == ChunkController.ChunkState.GeneratedData) {
                 lock(meshGenerationQueue) {
                     c.Value.cstate = ChunkController.ChunkState.GeneratingMesh;
                     meshGenerationQueue.Enqueue(c.Value);
                 }
            } else if (c.Value.cstate == ChunkController.ChunkState.GeneratedMesh) {
                c.Value.SetMesh();
            }
            
        }

    }

}
