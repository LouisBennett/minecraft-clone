﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshData
{
    private List<Vector3> _verticies;
    private List<Vector3> _normals;
    private List<int> _triangles;
    private List<Vector2> _uvs;

    private List<Color> _vertexColors;

    public List<Vector3> Verticies {get {return _verticies;}}
    public List<int> Triangles {get {return _triangles;}}
    public List<Vector2> UVs {get {return _uvs;}}
    public List<Color> VertexColors {get {return _vertexColors;}}

    public MeshData() {
        _verticies = new List<Vector3>();
        _triangles = new List<int>();
        _uvs = new List<Vector2>();
        _vertexColors = new List<Color>();
        _normals = new List<Vector3>();
    }
    public void AddQuad(Vector3 topLeft, Vector3 topRight, Vector3 bottomLeft, Vector3 bottomRight,Vector3 Direction) {
        int vCount = _verticies.Count;

        _verticies.Add(topLeft);
        _verticies.Add(topRight);
        _verticies.Add(bottomLeft);
        _verticies.Add(bottomRight);
        _triangles.Add(vCount+2);
        _triangles.Add(vCount+3);
        _triangles.Add(vCount);

        _triangles.Add(vCount+1);
        _triangles.Add(vCount);
        _triangles.Add(vCount+3);
    }
    public void AddUVs(Vector2[] uvs) {
        _uvs.AddRange(uvs);
    }
    public void ClearData() {
        _verticies.Clear();
        _triangles.Clear();
        _uvs.Clear();
        _vertexColors.Clear();
        _normals.Clear();
    }
    public void JoinMeshData(MeshData toJoin) {
        int vertexCount = _verticies.Count;
        _verticies.AddRange(toJoin.Verticies);
        _uvs.AddRange(toJoin.UVs);
        _vertexColors.AddRange(toJoin.VertexColors);
        foreach (int t in toJoin.Triangles) {
            _triangles.Add(vertexCount+t);
        }
    }
    public void AddOffset(Vector3 Offset) {
        for (int i = 0; i < _verticies.Count; i++)
        {
            _verticies[i] += Offset;
        }
    }
    public Mesh toMesh()
    {
        Mesh m = new Mesh();
        m.vertices = _verticies.ToArray();
        m.triangles = _triangles.ToArray();
        m.uv = _uvs.ToArray();
        m.colors = _vertexColors.ToArray();
        m.RecalculateNormals();
        m.RecalculateBounds();
        return m;
    }
}
