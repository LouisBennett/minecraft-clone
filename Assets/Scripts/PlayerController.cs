using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerController : MonoBehaviour
{
    public Transform Camera;
    public float MouseSensitivity;
    public float MoveSpeed;
    public float Gravity;
    public float JumpVelocity;
    public LayerMask GravityMask;

    public AnimationCurve jumpFallOff;
    public GameObject waila;
    float camRot;
    Vector3 velocity = new Vector3();

    bool isJumping = false;
    CharacterController characterController;

    float distToGround;
    // Start is called before the first frame update

    Vector3 mouseBlock = Vector3.negativeInfinity;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        characterController = GetComponent<CharacterController>();
       distToGround= characterController.bounds.extents.y;    
    }

    // Update is called once per frame
    void Update()
    {
        //magical mouse movement
        float mVoz = Input.GetAxis("Mouse X") * MouseSensitivity * Time.deltaTime;
        float mVert = Input.GetAxis("Mouse Y") * MouseSensitivity * Time.deltaTime;
        camRot -= mVert;
        camRot = Mathf.Clamp(camRot,-90f,90f);
        Camera.localRotation = Quaternion.Euler(camRot,0f,0f);
        transform.Rotate(Vector3.up*mVoz);

        //magical player movement
        float horizInput = Input.GetAxis("Horizontal") * MoveSpeed;
        float vertInput = Input.GetAxis("Vertical") * MoveSpeed;

        Vector3 forwardMovement = transform.forward * vertInput;
        Vector3 rightMovement = transform.right * horizInput;

        characterController.SimpleMove(forwardMovement + rightMovement);
        if (Input.GetKeyDown(KeyCode.Space)&&!isJumping){
            isJumping = true;
            StartCoroutine(JumpEvent());
        }
        MouseRay();
                Text tt = waila.GetComponentInChildren<Text>(true);
                tt.text = "";
        if (mouseBlock != Vector3.negativeInfinity) {
            byte b = WorldController.MainWorld.GetBlock(mouseBlock);
            if (b <255 && b>0) {
                Text t = waila.GetComponentInChildren<Text>(true);
                t.text = Block.Blocks[b].Name;
            }
        }
    }
    private void MouseRay() {
        Camera c = Camera.GetComponent<Camera>();
        Ray r = c.ViewportPointToRay(new Vector3(0.5f,0.5f,0f));
        RaycastHit rh;
        if (Physics.Raycast(r,out rh,10f,GravityMask)) {
            mouseBlock = rh.point;
        } else {
            mouseBlock = Vector3.negativeInfinity;
        }
    }
    private IEnumerator JumpEvent() {
        characterController.slopeLimit = 90f;
        float timeInAir =0.0f;
        do {
            float jumpForce = jumpFallOff.Evaluate(timeInAir);
            characterController.Move(Vector3.up * jumpForce * JumpVelocity * Time.deltaTime);
            timeInAir += Time.deltaTime;
            yield return null;

        } while (!characterController.isGrounded && characterController.collisionFlags != CollisionFlags.Above);
        isJumping = false;
    }
}
